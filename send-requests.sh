#!/bin/sh

export URL=http://$(oc get route version-app -n ab-demonstration -o template --template='{{.spec.host}}')

echo "============== Starting sending requests ======================"

while true 
do 
    sleep 1
    curl $URL
done
# AB Deployment Example

## Preparing the applications

first we have to setup the applications that will be running different versions of the same app. Since it is the first interation, they will be running the same version. You can test different versions by changing the source code.

To setup the apps:

    oc new-project ab-demonstration

    oc new-app --name version1  https://gitlab.com/openshift-samples/ab-deployment.git -n ab-demonstration

    oc new-app --name version2  https://gitlab.com/openshift-samples/ab-deployment.git -n ab-demonstration

To create the route that load balances the two services:

    oc create -f route.yml

## Testing

Run the send-request script that will send one request per second to the route. Since this AB is configured to 50/50,
you will see responses from both versions like this: 

    Reponse from POD: version2-1-kcbsw !!!
    Reponse from POD: version1-1-hkqpq !!!
    Reponse from POD: version2-1-kcbsw !!!
    Reponse from POD: version1-1-hkqpq !!!
    Reponse from POD: version2-1-kcbsw !!!

To run the script: 
    
    ./send-requests.sh 

You may stop it using ctrl+c .

## Proving the service discovery

While the script is running, open a new terminal window. We can demonstrate how service discovery works in OpenShift by escalating the apps:

    oc scale dc/version1 --replicas=2 -n ab-demonstration
    oc scale dc/version2 --replicas=2 -n ab-demonstration

Having more replicas means that new pods will respond to the same request    